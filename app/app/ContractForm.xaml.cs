﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for ContractForm.xaml
    /// </summary>
    public partial class ContractForm : Window
    {
        DBHelper dbHelper;
        MainWindow mw = new MainWindow();

        public ContractForm()
        {
            InitializeComponent();
            dbHelper = new DBHelper();

            #region fill comboboxes
            var cbList = new List<ComboBox> { rateCB, clientCB, branchCB,  InsuranceCB};
            var vwNames=new List<string>{"RATE_NAME_VW","CLIENT_NAME_VW","BRANCH_NAME_VW","INSURANCE_NAME_VW"};
            var names = cbList.Zip(vwNames, (cb,vw) => new {ComboBox = cb, String = vw});

            foreach(var cb in names){
                dbHelper.fillCB(mw.conn, cb.ComboBox, cb.String, "ID", "NAME");
            }
            #endregion 
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}

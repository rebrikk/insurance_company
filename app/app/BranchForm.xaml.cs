﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for BranchForm.xaml
    /// </summary>
    public partial class BranchForm : Window
    {
        DBHelper dbHelper;
        MainWindow mw = new MainWindow();

        public BranchForm()
        {
            InitializeComponent();
            dbHelper = new DBHelper();
            dbHelper.fillCB(mw.conn, cityCB, "CITY", "ID", "NAME");
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}

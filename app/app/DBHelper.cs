﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using System.Data;
using System.Windows.Controls;
using System.Data.Common;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace app
{
    public class DBHelper
    {
        public String connString;
        public DBHelper(){}

        // конструктор класса, входные параметры - имя юзера, пароль и имя бд 
        public DBHelper(String username, String password, String dataSource)
        {
            connString = "User Id=" + username + ";Password=" + password + ";Data Source=" + dataSource;
        }

        // соединиться с бд
        public void connect(ref OracleConnection conn)
        {
            conn = new OracleConnection();
            conn.ConnectionString = connString;
            conn.Open();
        }
        // добавить параметр типа объект
        public OracleParameter addPar(ParameterDirection direction, Object value, String udtTypeName)
        {
            OracleParameter par = new OracleParameter();
            par.OracleDbType = OracleDbType.Object;// тип параметра - объект
            par.Direction = direction;//направлние параметра (входной/выходной)
            par.UdtTypeName = udtTypeName; // название типа в бд
            par.Value = value; // значение
            return par;
        }
        // добавить строковый параметр
        public OracleParameter addPar(String value)
        {
            OracleParameter par = new OracleParameter();
            par.OracleDbType = OracleDbType.Varchar2;
            par.Direction = ParameterDirection.Input;
            par.Value = value;
            return par;
        }
        // добавить числовой параметр
        public OracleParameter addPar(Decimal value)
        {
            OracleParameter par = new OracleParameter();
            par.OracleDbType = OracleDbType.Decimal;
            par.Direction = ParameterDirection.Input;
            par.Value = value;
            return par;
        }
        //получить числовое значение выделенного пункта комбобокса
        public Decimal cbVal(ComboBox cb)
        {
            return Convert.ToDecimal(cb.SelectedValue);
        }
        //выставить данные договора
        public CONTRACT_O setContractData(ContractForm contractForm)
        {
            CONTRACT_O contract = new CONTRACT_O();
            contract.BRANCH_IDIsNull = false;
            contract.CLIENT_IDIsNull = false;
            contract.IDIsNull = false;
            contract.INSURANCE_IDIsNull = false;
            contract.RATE_IDIsNull = false;
            contract.SUMIsNull = false;

            contract.ID = 1;
            contract.BRANCH_ID = cbVal(contractForm.branchCB);
            contract.CLIENT_ID = cbVal(contractForm.clientCB);
            contract.INSURANCE_ID = cbVal(contractForm.InsuranceCB);
            contract.RATE_ID = cbVal(contractForm.rateCB);
            contract.SUM = Convert.ToDecimal(contractForm.sumTxt.Text);
            return contract;
        }
        public CONTRACT_O setContractData(DataGrid dg)
        {
            CONTRACT_O contract = new CONTRACT_O();
            contract.BRANCH_IDIsNull = false;
            contract.CLIENT_IDIsNull = false;
            contract.IDIsNull = false;
            contract.INSURANCE_IDIsNull = false;
            contract.RATE_IDIsNull = false;
            contract.SUMIsNull = false;

            contract.ID = 1;
            contract.BRANCH_ID = getCellDecimal(dg, 0);
            contract.CLIENT_ID = getCellDecimal(dg, 0);
            contract.INSURANCE_ID = getCellDecimal(dg, 0);
            contract.RATE_ID = getCellDecimal(dg, 0);
            contract.SUM = getCellDecimal(dg, 0);
            return contract;
        }
        // управлять договором (добавить/редактировать/рассторгнуть)
        public void manageContract(OracleConnection conn, DataGrid dg, String proc)
        {
            OracleParameter[] params_ = new OracleParameter[] { null,null};
            ContractForm contractForm = new ContractForm();
            if (proc == "TERMINATE")
                params_[1] = addPar(getCellDecimal(dg, 1));
            else
            {
                if (proc == "EDIT") fillContractForm(conn, dg, ref contractForm);
                contractForm.ShowDialog();
                params_[1] = addPar(ParameterDirection.Input, setContractData(contractForm), "CONTRACT_O");
            }
            exec(conn, "CONTRACT_PKG."+proc, params_, dg); // вызов процедуры
            fillDG(conn, dg, "CONTRACT_VW");
        }
        //выставить данные филиала
        BRANCH_O setBranchData(BranchForm branchForm)
        {
            BRANCH_O branch = new BRANCH_O();
            branch.CITY_IDIsNull = false;
            branch.IDIsNull = false;
            branch.ADDRESS = branchForm.addressTxt.Text;
            branch.CITY_ID = cbVal(branchForm.cityCB);
            branch.ID = 1;
            branch.NAME = branchForm.nameTxt.Text;
            branch.PHONE = branchForm.phoneTxt.Text;
            return branch;
        }
        //выставить идентификатор филиала
        BRANCH_O setBranchId(DataGrid dg,int columnNum)
        {
            BRANCH_O branch = new BRANCH_O();
            branch.CITY_IDIsNull = false;
            branch.IDIsNull = false;
            branch.ADDRESS = "";
            branch.CITY_ID = 0;
            branch.ID = getCellDecimal(dg,columnNum);
            branch.NAME = "";
            branch.PHONE = "";
            return branch;
        }
        BRANCH_O setBranchData(DataGrid dg)
        {
            BRANCH_O branch = new BRANCH_O();
            branch.CITY_IDIsNull = false;
            branch.IDIsNull = false;
            branch.ADDRESS = getCellStr(dg, 3); ;
            branch.CITY_ID = 0;
            branch.ID = getCellDecimal(dg, 0);
            branch.NAME = getCellStr(dg, 1); ;
            branch.PHONE = getCellStr(dg, 4); ;
            return branch;
        }
        public void editBranch(OracleConnection conn, DataGrid dg)
        {
            OracleParameter[] params_ = new OracleParameter[] { null, addPar(ParameterDirection.Input, setBranchData(dg), "BRANCH_O"), addPar("EDIT") };
            exec(conn, "ADM_PKG.MANAGE_BRANCH", params_, dg);
        }
        // управлять филиалом (добавить/редактировать/рассторгнуть)
        public void manageBranch(OracleConnection conn, DataGrid dg, String proc)
        {
            OracleParameter[] params_ = new OracleParameter[] { null,  null, addPar(proc)};
            BranchForm branchForm = new BranchForm();
            if (proc == "REMOVE")
                params_[1] = addPar(ParameterDirection.Input, setBranchId(dg,0), "BRANCH_O");
            else
            {
                if (proc == "EDIT") fillBranchForm(conn, dg,ref branchForm);
                branchForm.ShowDialog();
                params_[1] = addPar(ParameterDirection.Input, setBranchData(branchForm), "BRANCH_O");
            }
            exec(conn, "ADM_PKG.MANAGE_BRANCH", params_, dg);
            fillDG(conn, dg, "BRANCH_VW");
        }
        //выставить данные вида страхования
        INSURANCE_O setInsuranceData(InsuranceForm insurForm)
        {
            INSURANCE_O insurance = new INSURANCE_O();
            insurance.IDIsNull = false;
            insurance.DESCRIPTION = insurForm.descriptionTxt.Text;
            insurance.ID = 1;
            insurance.NAME = insurForm.nameTxt.Text;
            return insurance;
        }
        //выставить идентификатор вида страхования
        INSURANCE_O setInsuranceId(DataGrid dg, int columnNum)
        {
            INSURANCE_O insurance = new INSURANCE_O();
            insurance.IDIsNull = false;
            insurance.DESCRIPTION = "";
            insurance.ID = getCellDecimal(dg, columnNum); 
            insurance.NAME = "";
            return insurance;
        }
        //выставить данные вида страхования
        INSURANCE_O setInsuranceData(DataGrid dg)
        {
            INSURANCE_O insurance = new INSURANCE_O();
            insurance.IDIsNull = false;
            insurance.DESCRIPTION = getCellStr(dg, 2);
            insurance.ID = getCellDecimal(dg, 0); 
            insurance.NAME = getCellStr(dg, 1);
            return insurance;
        }
        public void editInsurance(OracleConnection conn, DataGrid dg)
        {
            OracleParameter[] params_ = new OracleParameter[] { null, addPar(ParameterDirection.Input, setInsuranceData(dg), "INSURANCE_O"), addPar("EDIT") };
            exec(conn, "ADM_PKG.MANAGE_INSURANCE", params_, dg);
        }
        // управлять видом страхования(добавить/редактировать/рассторгнуть)    
        public void manageInsurance(OracleConnection conn,  DataGrid dg, String proc)
        {
            OracleParameter[] params_ = new OracleParameter[]{null, null, addPar(proc)};
            InsuranceForm insurForm = new InsuranceForm();
            if (proc == "REMOVE")
                params_[1] = addPar(ParameterDirection.Input, setInsuranceId(dg, 0), "INSURANCE_O");
            else
            {
                if (proc == "EDIT") fillInsurForm(conn, dg, ref insurForm);
                insurForm.ShowDialog();
                params_[1] = addPar(ParameterDirection.Input, setInsuranceData(insurForm), "INSURANCE_O");
            }
            exec(conn, "ADM_PKG.MANAGE_INSURANCE", params_, dg);
            fillDG(conn, dg, "INSURANCE");
        }
        //выставить данные тарифной ставки
        RATE_O setRateData(RateForm rateForm){
            RATE_O rate = new RATE_O();
            rate.IDIsNull = false;
            rate.DESCRIPTION = rateForm.descriptionTxt.Text;
            rate.ID = 1;
            rate.NAME = rateForm.nameTxt.Text;
            return rate;
        }
        //выставить идентификатор тарифной ставки
        RATE_O setRateId(DataGrid dg, int columnNum)
        {
            RATE_O rate = new RATE_O();
            rate.IDIsNull = false;
            rate.DESCRIPTION = "";
            rate.ID = getCellDecimal(dg, columnNum);
            rate.NAME = "";
            return rate;
        }
        RATE_O setRateData(DataGrid dg)
        {
            RATE_O rate = new RATE_O();
            rate.IDIsNull = false;
            rate.DESCRIPTION = getCellStr(dg, 2);
            rate.ID = getCellDecimal(dg, 0); ;
            rate.NAME = getCellStr(dg, 1);
            return rate;
        }
        public void editRate(OracleConnection conn, DataGrid dg)
        {
            OracleParameter[] params_ = new OracleParameter[] { null, addPar(ParameterDirection.Input, setRateData(dg), "RATE_O"), addPar("EDIT") };
            exec(conn, "ADM_PKG.MANAGE_RATE", params_, dg);
        }
        // управлять ставкой (добавить/редактировать/рассторгнуть)
        public void manageRate(OracleConnection conn, DataGrid dg, String proc)
        {
            OracleParameter[] params_ = new OracleParameter[] { null, null, addPar(proc) };
            RateForm rateForm = new RateForm();
            if (proc == "REMOVE")
                params_[1] = addPar(ParameterDirection.Input, setRateId(dg, 0), "RATE_O");
            else
            {
                rateForm.ShowDialog();
                params_[1] = addPar(ParameterDirection.Input, setRateData(rateForm), "RATE_O");
            }
            exec(conn, "ADM_PKG.MANAGE_RATE", params_, dg);
            fillDG(conn, dg, "RATE");
        }
        //выставить данные клиента
        CLIENT_O setClientData(ClientForm clientForm)
        {
            CLIENT_O client = new CLIENT_O();
            client.BIRTHDATEIsNull = false;
            client.IDIsNull = false;
            client.PHONEIsNull = false;
            client.BIRTHDATE = (DateTime)clientForm.birthDate.SelectedDate;
            client.ID = 1;
            client.NAME = clientForm.fioTxt.Text;
            client.PHONE = Convert.ToDecimal(clientForm.phoneTxt.Text);
            return client;
        }
        //выставить идентификатор клиента
        CLIENT_O setClientId(DataGrid dg, int columnNum)
        {
            CLIENT_O client = new CLIENT_O();
            client.BIRTHDATEIsNull = false;
            client.IDIsNull = false;
            client.PHONEIsNull = false;
            client.BIRTHDATE = DateTime.Now;
            client.ID = getCellDecimal(dg, columnNum);
            client.NAME = "";
            client.PHONE = 1;
            return client;
        }
        CLIENT_O setClientData(DataGrid dg)
        {
            CLIENT_O client = new CLIENT_O();
            client.BIRTHDATEIsNull = false;
            client.IDIsNull = false;
            client.PHONEIsNull = false;
            client.BIRTHDATE = DateTime.Now;
            client.ID = getCellDecimal(dg, 0);
            client.NAME = getCellStr(dg, 1);
            client.PHONE = getCellDecimal(dg, 2);
            return client;
        }
        public void editClient(OracleConnection conn, DataGrid dg)
        {
            OracleParameter[] params_ = new OracleParameter[] { null, addPar(ParameterDirection.Input,setClientData(dg),"CLIENT_O"), addPar("EDIT") };
            exec(conn, "ADM_PKG.MANAGE_CLIENT", params_, dg);

        }
        // управлять клиентом (добавить/редактировать/рассторгнуть)
        public void manageClient(OracleConnection conn, DataGrid dg, String proc)
        {
            OracleParameter[] params_ = new OracleParameter[] { null, null, addPar(proc) };
            ClientForm clientForm = new ClientForm();
            if (proc == "REMOVE")
                params_[1] = addPar(ParameterDirection.Input, setRateId(dg, 0), "CLIENT_O");
            else
            {
                clientForm.ShowDialog();
                params_[1] = addPar(ParameterDirection.Input, setClientData(clientForm), "CLIENT_O");
            }
            exec(conn, "ADM_PKG.MANAGE_CLIENT", params_, dg);
            fillDG(conn, dg, "CLIENT");
        }
        // вызов функции
        public void exec(OracleConnection conn, String procedure, OracleParameter[] params_, DataGrid dg)
        {
            RESPONSE_O response = new RESPONSE_O(); //ответ функции
            response.CODEIsNull = false;
            response.CODE = 5; // код - результат выполнения
            response.DESCRIPTION = "xxx"; //описание результата

            params_[0] = addPar(ParameterDirection.ReturnValue, new RESPONSE_O(), "RESPONSE_O");// параметр - ответ

            OracleCommand command = new OracleCommand(procedure, conn);
            command.CommandType = CommandType.StoredProcedure;
            for (int i = 0; i < params_.Length; i++)
            {
                command.Parameters.Add(params_[i]);
            }
            command.ExecuteNonQuery(); // вызов функции
            response = (RESPONSE_O)command.Parameters[0].Value; // берем возвращаемое значение
            if (response.CODE != 0) { MessageBox.Show(response.DESCRIPTION); return; }
            MessageBox.Show(response.DESCRIPTION);
        }

        // заполнить combobox, параметры: коннект к бд, комбобокс, таблица(из которой данные берем, значение пункта, наименование пункта комбобокса)
        public void fillCB(OracleConnection conn, ComboBox cb, string table, string SelectedValuePath, string DisplayedMemberPath)
        {
            OracleDataAdapter adapter = new OracleDataAdapter(new OracleCommand("select * from " + table, conn)); // считываем набор данных с таблицы
            DataSet set = new DataSet();
            adapter.Fill(set); // берем себе этот набор данных
            cb.ItemsSource = set.Tables[0].DefaultView; // устанавливаем этот набор источником данных для комбобокса
            cb.DisplayMemberPath = DisplayedMemberPath; // то, что отображаться будет в комбобоксе
            cb.SelectedValuePath = SelectedValuePath;
            cb.SelectedIndex = 0;
        }

        //заполнить таблицу
        public void fillDG(OracleConnection conn, DataGrid dataGrid, String source)
        {
            OracleDataAdapter adapter = new OracleDataAdapter("select * from " + source, conn);// считываем набор данных с таблицы
            DataSet set = new DataSet();
            adapter.Fill(set);
            dataGrid.ItemsSource = set.Tables[0].DefaultView; // устанавливаем этот набор источником данных для комбобокса
        }

        public decimal getCellDecimal(DataGrid dg, int colomnnum) // взять числовое значение из выделенной ячейки таблицы (на форме, не из бд)
        {
            var selectedrow = dg.SelectedItem;
            decimal value = 0;
            string cellvalue = (dg.SelectedCells[colomnnum].Column.GetCellContent(selectedrow) as TextBlock).Text;
            bool converted = decimal.TryParse(cellvalue, out value);
            if (!converted) return -1;
            return value;
        }

        public string getCellStr(DataGrid dg, int colomnnum)// взять строковое значение из выделенной ячейки таблицы (на форме, не из бд)
        {
            var selectedrow = dg.SelectedItem;
            string value = "";
            value = (dg.SelectedCells[colomnnum].Column.GetCellContent(selectedrow) as TextBlock).Text;
            return value;
        }

        public int dgVal(DataGrid dg, ComboBox cb, int columnNum)
        {
            return cb.Items.IndexOf(cb.FindName(getCellStr(dg, columnNum)));
        }

        public void fillContractForm(OracleConnection conn, DataGrid dg,ref ContractForm contractForm) // заполнить форму договора (клиент, ставка, сумма и тд)
        {
            contractForm.sumTxt.Text = getCellStr(dg, 2);
            contractForm.rateCB.SelectedItem = dgVal(dg, contractForm.rateCB, 3);
            contractForm.branchCB.SelectedItem = dgVal(dg, contractForm.branchCB, 4);
            contractForm.InsuranceCB.SelectedItem = dgVal(dg, contractForm.InsuranceCB, 5);
            contractForm.clientCB.SelectedItem = dgVal(dg, contractForm.clientCB, 6);
        }

        public void fillBranchForm(OracleConnection conn, DataGrid dg, ref BranchForm branchForm)// заполнить форму договора 
        {
            branchForm.nameTxt.Text = getCellStr(dg, 1);
            branchForm.phoneTxt.Text = getCellStr(dg, 2);
            branchForm.addressTxt.Text = getCellStr(dg, 3);
            branchForm.cityCB.SelectedItem = dgVal(dg, branchForm.cityCB, 1);
        }

        public void fillInsurForm(OracleConnection conn, DataGrid dg, ref InsuranceForm insurForm)// заполнить форму вида страхования
        {
            insurForm.nameTxt.Text = getCellStr(dg, 1);
            insurForm.descriptionTxt.Text = getCellStr(dg, 2);
        }

        public void fillRateForm(OracleConnection conn, DataGrid dg, ref RateForm rateForm)// заполнить форму ставки 
        {
            rateForm.nameTxt.Text = getCellStr(dg, 1);
            rateForm.descriptionTxt.Text = getCellStr(dg, 2);
        }

        public void fillClientForm(OracleConnection conn, DataGrid dg,ref ClientForm clientForm)// заполнить форму клиента 
        {
            clientForm.fioTxt.Text = getCellStr(dg, 1);
            clientForm.phoneTxt.Text = getCellStr(dg, 2);
            clientForm.birthDate.SelectedDate = DateTime.Now;
        }

    }
}

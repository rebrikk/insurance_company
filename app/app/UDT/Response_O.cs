//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace app {
    using System;
    using Oracle.DataAccess.Client;
    using Oracle.DataAccess.Types;
    using System.Xml.Serialization;
    using System.Xml.Schema;
    
    
    public class RESPONSE_O : INullable, IOracleCustomType, IXmlSerializable {
        
        private bool m_IsNull;
        
        private string m_DESCRIPTION;
        
        private decimal m_CODE;
        
        private bool m_CODEIsNull;
        
        public RESPONSE_O() {
            // TODO : Add code to initialise the object
            this.m_CODEIsNull = true;
        }
        
        public RESPONSE_O(string str) {
            // TODO : Add code to initialise the object based on the given string 
        }
        
        public virtual bool IsNull {
            get {
                return this.m_IsNull;
            }
        }
        
        public static RESPONSE_O Null {
            get {
                RESPONSE_O obj = new RESPONSE_O();
                obj.m_IsNull = true;
                return obj;
            }
        }
        
        [OracleObjectMappingAttribute("DESCRIPTION")]
        public string DESCRIPTION {
            get {
                return this.m_DESCRIPTION;
            }
            set {
                this.m_DESCRIPTION = value;
            }
        }
        
        [OracleObjectMappingAttribute("CODE")]
        public decimal CODE {
            get {
                return this.m_CODE;
            }
            set {
                this.m_CODE = value;
            }
        }
        
        public bool CODEIsNull {
            get {
                return this.m_CODEIsNull;
            }
            set {
                this.m_CODEIsNull = value;
            }
        }
        
        public virtual void FromCustomObject(Oracle.DataAccess.Client.OracleConnection con, System.IntPtr pUdt) {
            Oracle.DataAccess.Types.OracleUdt.SetValue(con, pUdt, "DESCRIPTION", this.DESCRIPTION);
            if ((CODEIsNull == false)) {
                Oracle.DataAccess.Types.OracleUdt.SetValue(con, pUdt, "CODE", this.CODE);
            }
        }
        
        public virtual void ToCustomObject(Oracle.DataAccess.Client.OracleConnection con, System.IntPtr pUdt) {
            this.DESCRIPTION = ((string)(Oracle.DataAccess.Types.OracleUdt.GetValue(con, pUdt, "DESCRIPTION")));
            this.CODEIsNull = Oracle.DataAccess.Types.OracleUdt.IsDBNull(con, pUdt, "CODE");
            if ((CODEIsNull == false)) {
                this.CODE = ((decimal)(Oracle.DataAccess.Types.OracleUdt.GetValue(con, pUdt, "CODE")));
            }
        }
        
        public virtual void ReadXml(System.Xml.XmlReader reader) {
            // TODO : Read Serialized Xml Data
        }
        
        public virtual void WriteXml(System.Xml.XmlWriter writer) {
            // TODO : Serialize object to xml data
        }
        
        public virtual XmlSchema GetSchema() {
            // TODO : Implement GetSchema
            return null;
        }
        
        public override string ToString() {
            // TODO : Return a string that represents the current object
            return "";
        }
        
        public static RESPONSE_O Parse(string str) {
            // TODO : Add code needed to parse the string and get the object represented by the string
            return new RESPONSE_O();
        }
    }
    
    // Factory to create an object for the above class
    [OracleCustomTypeMappingAttribute("GYREEV.RESPONSE_O")]
    public class RESPONSE_OFactory : IOracleCustomTypeFactory {
        
        public virtual IOracleCustomType CreateObject() {
            RESPONSE_O obj = new RESPONSE_O();
            return obj;
        }
    }
}

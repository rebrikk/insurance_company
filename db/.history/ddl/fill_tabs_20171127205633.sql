DECLARE
   TYPE BRANCH_TAB IS TABLE OF BRANCH%ROWTYPE INDEX BY NUMBER;
   vtBranches BRANCH_TAB;
   vCityId CITY.ID%TYPE;

   TYPE CLIENT_TAB IS TABLE OF CLIENT%ROWTYPE INDEX BY NUMBER;
   vtClients CLIENT_TAB;
BEGIN
    -------------------
    INSERT INTO INSURANSE VALUES  ID_SEQ.NEXTVAL, 'Страхование имущества', 'description';
    INSERT INTO INSURANSE VALUES  ID_SEQ.NEXTVAL, 'Страхование ответственности', 'description';
    INSERT INTO INSURANSE VALUES  ID_SEQ.NEXTVAL, 'Личное страхование', 'description';

    -------------------
    INSERT INTO CITY VALUES ID_SEQ.NEXTVAL, 'Москва';
    INSERT INTO CITY VALUES ID_SEQ.NEXTVAL, 'Новосибирск';
    INSERT INTO CITY VALUES ID_SEQ.NEXTVAL, 'Бийск';

    -------------------
    vtBranches(1).NAME = 'Филиал 1';
    vtBranches(2).NAME = 'Филиал 2';
    vtBranches(3).NAME = 'Филиал 3';
    vCityId := ID_SEQ.CURRVAL;

    FOR I IN 1..3 LOOP
       vtBranches(I).ID = ID_SEQ.NEXTVAL;
       vtBranches(I).CITY_ID = vCityId;
       vtBranches(I).ADDRESS = 'Дуси Ковальчук 191';
       vtBranches(I).PHONE  = '123456';
    END LOOP;

    FORALL I IN 1..3 LOOP
      INSERT INTO BRANCH VALUES  vtBranches(I);
    END LOOP;

   -------------------
   INSERT INTO RATE VALUES ID_SEQ.NEXTVAL, 'Формула 1', 'description';
   INSERT INTO RATE VALUES ID_SEQ.NEXTVAL, 'Формула 2', 'description';

   -------------------
   vtClients(1).NAME = 'Иванов Иван Иванович';
   vtClients(2).NAME = 'Петров Петр Петрович';
   vtClients(3).NAME = 'Васильев Василий Васильевич';

   FOR I IN 1..3 LOOP
       vtClients(I).ID = ID_SEQ.NEXTVAL;
       vtClients(I).BIRTHDATE = SYSDATE;
       vtClients(I).PHONE  = '123456';
    END LOOP;

    FORALL I IN 1..3 LOOP
      INSERT INTO CLIENT VALUES  vtClients(I);
    END LOOP;

   COMMIT;
END;
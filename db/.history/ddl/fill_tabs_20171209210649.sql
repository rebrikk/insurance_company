﻿DECLARE
   vCityId CITY.ID%TYPE;
BEGIN
    -------------------
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Страование имущества', 'description', NULL);
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Страхование жизни', 'description', NULL);
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, '������ �����������', 'description', NULL);

    -------------------
    INSERT INTO CITY VALUES (ID_SEQ.NEXTVAL, '������');
    INSERT INTO CITY VALUES (ID_SEQ.NEXTVAL, '�����������');
    INSERT INTO CITY VALUES (ID_SEQ.NEXTVAL, '�����');

    -------------------
    vCityId := ID_SEQ.CURRVAL;
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'������ 1',vCityId,'���� ��������� 191','123456', NULL);
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'������ 2',vCityId,'���� ��������� 192','123456', NULL);
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'������ 3',vCityId,'���� ��������� 193','123456', NULL);

   -------------------
   INSERT INTO RATE VALUES (ID_SEQ.NEXTVAL, '������� 1', 'description', NULL);
   INSERT INTO RATE VALUES (ID_SEQ.NEXTVAL, '������� 2', 'description', NULL);

   -------------------
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'������ ���� ��������','123456',SYSDATE);
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'������ ���� ��������','123456',SYSDATE);
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'�������� ������� ����������','123456',SYSDATE);

   COMMIT;
END;
/
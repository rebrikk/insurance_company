﻿DECLARE
   vCityId CITY.ID%TYPE := 0;
BEGIN
    -------------------
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Страхование имущества', 'Описание', NULL);
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Страхование жизни', 'Описание', NULL);
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Страхование бизнеса', 'Описание', NULL);
    -------------------
    INSERT INTO CITY VALUES (999999999, 'Новосибирск');
    INSERT INTO CITY VALUES (ID_SEQ.NEXTVAL, 'Москва');
    INSERT INTO CITY VALUES (ID_SEQ.NEXTVAL, 'Бийск');
    -------------------
    vCityId := ID_SEQ.CURRVAL;
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'Филиал 1',vCityId,'Дуси Ковальчук 191','123456', NULL);
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'Филиал 2',vCityId,'Дуси Ковальчук 192','123456', NULL);
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'Филиал 3',vCityId,'Дуси Ковальчук 193','123456', NULL);
   -------------------
   INSERT INTO RATE VALUES (ID_SEQ.NEXTVAL, 'Формула 1', 'Описание', NULL);
   INSERT INTO RATE VALUES (ID_SEQ.NEXTVAL, 'Формула 2', 'Описание', NULL);
   -------------------
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'Иванов Иван Иванович','123456',SYSDATE);
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'Васильев Василий Васильевич','123456',SYSDATE);
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'Рбрикова Татьяна Николаевна','123456',SYSDATE);

   COMMIT;
END;
/
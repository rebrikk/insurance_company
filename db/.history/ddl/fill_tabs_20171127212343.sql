DECLARE
   vCityId CITY.ID%TYPE;
BEGIN
    -------------------
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Страхование имущества', 'description', NULL);
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Страхование ответственности', 'description', NULL);
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Личное страхование', 'description', NULL);

    -------------------
    INSERT INTO CITY VALUES (ID_SEQ.NEXTVAL, 'Москва');
    INSERT INTO CITY VALUES (ID_SEQ.NEXTVAL, 'Новосибирск');
    INSERT INTO CITY VALUES (ID_SEQ.NEXTVAL, 'Бийск');

    -------------------
    vCityId := ID_SEQ.CURRVAL;
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'Филиал 1',vCityId,'Дуси Ковальчук 191','123456', NULL);
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'Филиал 2',vCityId,'Дуси Ковальчук 192','123456', NULL);
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'Филиал 3',vCityId,'Дуси Ковальчук 193','123456', NULL);

   -------------------
   INSERT INTO RATE VALUES (ID_SEQ.NEXTVAL, 'Формула 1', 'description', NULL);
   INSERT INTO RATE VALUES (ID_SEQ.NEXTVAL, 'Формула 2', 'description', NULL);

   -------------------
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'Иванов Иван Иванович','123456',SYSDATE);
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'Петров Петр Петрович','123456',SYSDATE);
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'Васильев Василий Васильевич','123456',SYSDATE);

   COMMIT;
END;
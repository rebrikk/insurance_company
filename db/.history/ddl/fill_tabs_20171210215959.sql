﻿BEGIN
    -------------------
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Страхование имущества', 'Описание', 'N');
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Страхование жизни', 'Описание', 'N');
    INSERT INTO INSURANCE VALUES  (ID_SEQ.NEXTVAL, 'Страхование бизнеса', 'Описание', 'N');
    -------------------
    INSERT INTO CITY VALUES (999999999, 'Новосибирск');
    INSERT INTO CITY VALUES (999999998, 'Москва');
    INSERT INTO CITY VALUES (999999997, 'Бийск');
    -------------------
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'Филиал 1',999999999,'Дуси Ковальчук 191','123456', 'N');
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'Филиал 2',999999998,'Дуси Ковальчук 192','123456', 'N');
    INSERT INTO BRANCH VALUES (ID_SEQ.NEXTVAL,'Филиал 3',999999997,'Дуси Ковальчук 193','123456', 'N');
   -------------------
   INSERT INTO RATE VALUES (ID_SEQ.NEXTVAL, 'Формула 1', 'Описание', 'N');
   INSERT INTO RATE VALUES (ID_SEQ.NEXTVAL, 'Формула 2', 'Описание', 'N');
   -------------------
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'Иванов Иван Иванович','123456',SYSDATE);
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'Васильев Василий Васильевич','123456',SYSDATE);
   INSERT INTO CLIENT VALUES (ID_SEQ.NEXTVAL,'Рбрикова Татьяна Николаевна','123456',SYSDATE);

   COMMIT;
END
/
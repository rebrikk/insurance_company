CREATE OR REPLACE PACKAGE BRANCH_PKG IS
    -- пакет управления филиалом
   
   FUNCTION CREATE_(po$Branch IN BRANCH_O) RETURN RESPONSE_O; -- создать 

   FUNCTION EDIT(po$Branch IN BRANCH_O) RETURN RESPONSE_O; -- редактировать

   FUNCTION REMOVE(p#Id NUMBER) RETURN RESPONSE_O; -- сделать неактуальным(неиспользуемым)

END BRANCH_PKG;
/
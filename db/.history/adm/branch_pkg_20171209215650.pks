CREATE OR REPLACE PACKAGE BRANCH_PKG IS
    -- пакет управления филиалом
   
   FUNCTION CREATE_(po$Branch IN BRANCH_O) RETURN RESPONSE_O; -- создать 

   FUNCTION EDIT(po$Branch IN BRANCH_O) RETURN RESPONSE_O;

   FUNCTION REMOVE(p#Id NUMBER) RETURN RESPONSE_O;

END BRANCH_PKG;
/
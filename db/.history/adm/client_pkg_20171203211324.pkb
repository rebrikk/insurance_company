CREATE OR REPLACE PACKAGE BODY CLIENT_PKG IS
   
   FUNCTION CREATE_(pr$Client IN CLIENT_O) RETURN RESPONSE_O IS
   BEGIN
      INSERT INTO CLIENT VALUES pr$Client;

      COMMIT;
      RETURN NEW RESPONSE_O(CODE => 0, DESCRIPTION => 'Client created');

      EXCEPTION 
        WHEN OTHERS THEN
          RETURN NEW RESPONSE_O(CODE => SQLCODE, DESCRIPTION => SQLERRM);
   END CREATE_;

   FUNCTION EDIT(pr$Client IN CLIENT_O) RETURN RESPONSE_O IS
   BEGIN
    UPDATE CLIENT
    SET NAME = Client.NAME,
       PHONE = Client.PHONE,
       BIRTHDATE = Client.BIRTHDATE
       WHERE ID = Client.ID;
    
    COMMIT;
      RETURN NEW RESPONSE_O(CODE => 0, DESCRIPTION => 'Client updated');

      EXCEPTION 
        WHEN OTHERS THEN
          RETURN NEW RESPONSE_O(CODE => SQLCODE, DESCRIPTION => SQLERRM);
   END EDIT;

   FUNCTION REMOVE(p#Id NUMBER) RETURN RESPONSE_O IS
   BEGIN
    DELETE FROM CLIENT
    WHERE ID = p#Id;

    COMMIT;
      RETURN NEW RESPONSE_O(CODE => 0, DESCRIPTION => 'Client deleted');

      EXCEPTION 
        WHEN OTHERS THEN
          RETURN NEW RESPONSE_O(CODE => SQLCODE, DESCRIPTION => SQLERRM);
   END REMOVE;

END CLIENT_PKG;
/
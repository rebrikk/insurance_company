CREATE OR REPLACE PACKAGE ADM_PKG IS
    -- административный пакет

   FUNCTION MANAGE_BRANCH(
       po$Branch IN BRANCH_O,
       p$Action  IN VARCHAR2
    ) RETURN RESPONSE_O;

    FUNCTION MANAGE_CLIENT(
       po$Client IN CLIENT_O,
       p$Action  IN VARCHAR2
    ) RETURN RESPONSE_O;

    FUNCTION MANAGE_INSURANCE(
       po$Insurance IN INSURANCE_O,
       p$Action  IN VARCHAR2
    ) RETURN RESPONSE_O;

    FUNCTION MANAGE_RATE(
       po$Rate IN RATE_O,
       p$Action  IN VARCHAR2
    ) RETURN RESPONSE_O;

END ADM_PKG;
/
CREATE OR REPLACE TYPE CLIENT_O AS OBJECT(
   NAME         VARCHAR2(120),
   PHONE        NUMBER(11),
   BIRTHDATE    DATE
);
/
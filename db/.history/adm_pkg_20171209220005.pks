CREATE OR REPLACE PACKAGE ADM_PKG IS
    -- административный пакет

   FUNCTION MANAGE_BRANCH( -- управлять филиалом
       po$Branch IN BRANCH_O,
       p$Action  IN VARCHAR2
    ) RETURN RESPONSE_O;

    FUNCTION MANAGE_CLIENT( -- управлять кл
       po$Client IN CLIENT_O,
       p$Action  IN VARCHAR2
    ) RETURN RESPONSE_O;

    FUNCTION MANAGE_INSURANCE( -- управлять 
       po$Insurance IN INSURANCE_O,
       p$Action  IN VARCHAR2
    ) RETURN RESPONSE_O;

    FUNCTION MANAGE_RATE( -- управлять 
       po$Rate IN RATE_O,
       p$Action  IN VARCHAR2
    ) RETURN RESPONSE_O;

END ADM_PKG;
/
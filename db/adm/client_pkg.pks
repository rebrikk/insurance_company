CREATE OR REPLACE PACKAGE CLIENT_PKG IS
    -- пакет управления клиентом
   
   FUNCTION CREATE_(po$Client IN CLIENT_O) RETURN RESPONSE_O;-- создать 

   FUNCTION EDIT(po$Client IN CLIENT_O) RETURN RESPONSE_O;--рудактировать

   FUNCTION REMOVE(p#Id NUMBER) RETURN RESPONSE_O; -- сделать неактуальным(неиспользуемым)

END CLIENT_PKG;
/
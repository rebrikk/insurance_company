CREATE OR REPLACE PACKAGE RATE_PKG IS
    -- пакет управления ставкой тарифной
   
   FUNCTION CREATE_(po$Rate IN RATE_O) RETURN RESPONSE_O;-- создать 

   FUNCTION EDIT(po$Rate IN RATE_O) RETURN RESPONSE_O; -- редактировать

   FUNCTION REMOVE(p#Id NUMBER) RETURN RESPONSE_O; -- сделать неактуальным(неиспользуемым)

END RATE_PKG;
/
CREATE OR REPLACE TYPE INSURANCE_O AS OBJECT(
    ID          NUMBER(10),
   NAME        VARCHAR2(100),
   DESCRIPTION VARCHAR2(500)
);
/
create user gyreev identified by gyreev default tablespace users temporary tablespace temp
/
grant create session to gyreev 
/
grant create procedure to gyreev
/
grant create table to gyreev
/
grant create type to gyreev
/
grant create view to gyreev
/
grant create trigger to gyreev
/
grant create sequence to gyreev
/
alter user gyreev quota 100m on users
/
grant alter any table to gyreev
/
grant alter any procedure to gyreev
/
grant alter any trigger to gyreev
/
grant alter profile to gyreev
/
grant delete any table to gyreev
/
grant drop any table to gyreev
/
grant drop any procedure to gyreev
/
grant drop any trigger to gyreev
/
grant drop any view to gyreev
/
grant drop profile to gyreev
/
grant debug connect session to gyreev
/
grant debug any procedure to gyreev
/

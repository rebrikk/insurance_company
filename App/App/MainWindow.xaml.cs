﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Oracle.DataAccess.Client;
using System.Data;

namespace app
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        DBHelper dbHelper;
        public OracleConnection conn;

        public MainWindow()
        {
            InitializeComponent();
            dbHelper = new DBHelper("GYREEV", "gyreev", "localhost:1521/XE");
            dbHelper.connect(ref conn);

            #region fill grids
            var gridList = new List<DataGrid> { contractGrid, clientGrid, branchGrid, rateGrid, insuranceGrid };
            var vwNames = new List<string> { "CONTRACT_VW", "CLIENT", "BRANCH_VW", "RATE", "INSURANCE" };
            var names = gridList.Zip(vwNames, (dg, vw) => new { DataGrid = dg, String = vw });

            foreach (var dg in names)
            {
                dbHelper.fillDG(conn, dg.DataGrid, dg.String);
            }
            #endregion 
            editContract.Visibility = Visibility.Hidden;
            editClient.Visibility = Visibility.Hidden;
            editbranch.Visibility = Visibility.Hidden;

        }

        private void createContract_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageContract(conn, contractGrid, "CREATE_");
        }

        private void editContract_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageContract(conn, contractGrid, "EDIT");
        }

        private void terminateContract_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageContract(conn, contractGrid, "TERMINATE");
        }

        private void createClient_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageClient(conn, clientGrid, "CREATE");
        }

        private void editClient_Click(object sender, RoutedEventArgs e)
        {
           // dbHelper.manageClient(conn, clientGrid, "EDIT");
            dbHelper.editClient(conn, clientGrid);
        }

        private void createbranch_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageBranch(conn, branchGrid, "CREATE");
        }

        private void editbranch_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageBranch(conn, branchGrid, "EDIT");
        }

        private void setUnusedbranch_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageBranch(conn, branchGrid, "REMOVE");
        }

        private void createRate_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageRate(conn, rateGrid, "CREATE");
        }

        private void editRate_Click(object sender, RoutedEventArgs e)
        {
            //dbHelper.manageRate(conn, rateGrid, "CREATE");
            dbHelper.editRate(conn, rateGrid);
        }

        private void setUnusedRate_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageRate(conn, rateGrid, "REMOVE");
        }

        private void createInsurance_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageInsurance(conn, insuranceGrid, "CREATE");
        }

        private void editInsurance_Click(object sender, RoutedEventArgs e)
        {
            //dbHelper.manageInsurance(conn, insuranceGrid, "EDIT");
            dbHelper.editInsurance(conn, insuranceGrid);
        }

        private void setUnusedInsurance_Click(object sender, RoutedEventArgs e)
        {
            dbHelper.manageInsurance(conn, insuranceGrid, "REMOVE");
        }
    }
}

